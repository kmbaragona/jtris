function equals(a,b){
    //console.log(JSON.stringify(a));
    //console.log(JSON.stringify(b));
    if(a.length != b.length){
        return 0;
    }
    for(var i=a.length-1;i>=0;i--){
        if(a[i][0]!=b[i][0]){
           return 0;
        }
        if(a[i][1]!=b[i][1]){
           return 0;
        }
    }
    return 1;
    //return JSON.stringify(a) === JSON.stringify(b);
}
function transpose(a) {
    return Object.keys(a[0]).map(
        function (c) { return a.map(function (r) { return r[c]; }); }
    );
}

Array.max = function( array ){
    return Math.max.apply( Math, array );
};
Array.min = function( array ){
    return Math.min.apply( Math, array );
};
function udlr(p){
   var x=p[0];
   var y=p[1];
   return [[x+1,y],[x-1,y],[x,y+1],[x,y-1]];
}
function ptadd(p1,p2){
    return [p1[0]+p2[0],p1[1]+p2[1]];
}
function ptdiff(p1,p2){
    return [p1[0]-p2[0],p1[1]-p2[1]];
}
function r90(pt){
    return [-pt[1],pt[0]];
}
function r90about(pt,about){
    return ptadd(r90(ptdiff(pt,about)),about);
}
function rotations(pts){
     var rots=[];
     var current=pts;

    for(var i=0;i<3;i++){

       current = current.map(function(p){return r90(p)});
       rots.push(current);
    }
    return rots;
}
function getCenterOfMass(pts){
    var xsum=0;var ysum=0;
    for (pi in pts){
        pt=pts[pi];
        xsum+=pt[0];
        ysum+=pt[1];
    }
    xsum/=pts.length;
    ysum/=pts.length;
    //var center = [Math.floor(xsum),Math.ceil(ysum)];
    var center=[xsum,ysum]
    return center;
}
function getDenormalAmount(pts){
    var mx=Array.min(pts.map(function(p){return p[0]}));
    var my=Array.min(pts.map(function(p){return p[1]}));
    return [mx,my];
}
function normalize(pts){
    var denormal=getDenormalAmount(pts);
    return pts.map(function(p){return ptdiff(p,denormal)}).sort(function(a,b){
        if((a[0]-b[0]) !=0){
           return a[0]-b[0];
        }
        return a[1]-b[1];
    });
}
function uniqpieces(pieces){
    var uniqs=[];

    for (idx in pieces){
         var piece = pieces[idx];
         var rots=rotations(piece);
         var alreadyhave=0;

         for (ri in rots){
            //console.log(JSON.stringify(rots[ri]));
            for (ui in uniqs){
                var n = normalize(rots[ri]);

                if(equals(uniqs[ui],n)){
                    alreadyhave=1;
                }
            }
         }
        if(alreadyhave==0){
            uniqs.push(normalize(piece));
        }

    }
    return uniqs;
}
function addpiece(piece){

    var newpieces = [];
    var testedMinors=[];
    for (pi in piece){
        var around=piece[pi];
        var minor=JSON.parse(JSON.stringify(piece));
        minor.splice(pi,1);
        if(uniqpieces(testedMinors.concat([minor])).length == testedMinors.length){
            continue;
        }else{
            testedMinors.push(minor);
        }
        var neighbors = udlr(around);

        for (ni in neighbors){
           var n = neighbors[ni];

           var alreadyhave=0;
           for (idx in piece){
              var p = piece[idx];
               if(n[0]==p[0]&&n[1]==p[1]){
                   alreadyhave=1;
               }
           }
            if(alreadyhave==0){

               newpieces.push(piece.concat([n]));
            }
        }
    }

    return uniqpieces(newpieces);
}
function genpieces(length){
    var current=[[[0,0]]];
    for (var i=1;i<length;i++){
        var thisgen=[];
        for (var pi in current){
           var nw=addpiece(current[pi]);
           thisgen=thisgen.concat(nw);
        }
        current = uniqpieces(thisgen);
    }
    return current;
}

function gridReduce(l,r,reducer){
    var result=[];
    for(x in l){
        if(!result[x]){
           result[x]=[];
        }
        for(y in l[x]){
            result[x][y]=reducer(l[x][y],r[x][y]);
        }
    }
    return result;
}
function gridSum(grid){
   var sum=0;
    for(x in grid){
        for(y in grid[x]){
            sum+=grid[x][y];
        }
    }
   return sum;
}

function gridPaint(grid,paintcells,offsetFloat){
    var offset=roundPt(offsetFloat);
    var ng=JSON.parse(JSON.stringify(grid));
    for (pi in paintcells){
         var c=paintcells[pi];
         var x=c[0]+offset[0];
         var y=c[1]+offset[1];
        if(ng.hasOwnProperty(x)){
            if(ng[x].hasOwnProperty(y)){
                 ng[x][y]=1;
            }
        }
    }
    return ng;
}
function roundPt(pt){
    return pt.map(function(i){return Math.round(i)})
}
function roundPiece(piece){
    return piece.map(function(p){return roundPt(p)});
}
function clearRows(cells){
    var ng=transpose(cells);
    var cleared=0;
    for (var r=0;r<ng.length;r++){
        var row=ng[r];
        var prod=1;
        for(var c=0;c<row.length;c++){
            prod*=row[c];
        }
        //console.log(prod);
        if(prod){
            cleared++;
            for(var rr=r;rr>=0;rr--){
                var copyrow;
                if((rr-1)>=0){
                    copyrow=ng[rr-1];
                }else{
                    copyrow=[];
                    for (j in row){
                        copyrow.push(0);
                    }
                }
                ng[rr]=copyrow;
            }
        }
    }
    return [transpose(ng),cleared];
}
function gravity(cells){
    var count=0;
    for(var x=0;x<cells.length;x++){
        for(var y=0;y<cells[x].length-1;y++){
            var here=cells[x][y];
            if(here){
                var supported=0;
                if(x-1>=0 && cells[x-1][y])
                    supported=1;
                if(x+1<cells.length && cells[x+1][y])
                    supported=1;
                if(y-1>=0 && cells[x][y-1])
                    supported=1;
                if(cells[x][y+1])
                    supported=1;
                if(!supported){
                    cells[x][y]=0;
                    cells[x][y+1]=1;
                    count++;
                }
            }
        }
    }
    return count;
}
function gravityClear(cells){
    var changed=1;
    var cleared=0;
    while(changed){
        changed=0;
        var result = clearRows(cells);
        cleared+=result[1];
        changed+=result[1];
        cells=result[0];
        changed+=gravity(cells);
    }
    return [cells,cleared];
}
function doesItCollide(cellsolid,piece,posFloat,w,h){
    //var piece=roundPiece(pieceFloat);
    //console.log(piece);
    var pos = roundPt(posFloat);
    var piecegrid=gridPaint(initArr([],w,h),piece,pos);
    var prod=gridSum(gridReduce(cellsolid,piecegrid,function(l,r){return l*r}));
    var pieceH=Array.max(piece.map(function(p){return p[1]}))+1;
    var pieceW=Array.max(piece.map(function(p){return p[0]}))+1;
    var maxPieceY=pieceH+pos[1];
    var maxPieceX=pieceW+pos[0];
    var hitBottom=maxPieceY>h?1:0;
    var hitRight=maxPieceX>w?1:0;
    var hitLeft=pos[0]<0?1:0;
    if(prod || hitBottom || hitRight || hitLeft){
        return 1;
    }else{
        return 0;
    }
}

var globtable;

function reflowLayout(){
    // Get view height
    var viewportHeight = document.documentElement.clientHeight;
    var viewportWidth = document.documentElement.clientWidth;
    if(globtable){
        var h=viewportHeight-5;
        globtable.css('height',h+'px');
        var w=(globtable.data('w')/globtable.data('h')*h);
        globtable.css('width',w+'px');
        globtable.css('top',((viewportHeight-h)/2)+'px');
        globtable.css('left',((viewportWidth-w)/2)+'px');
    }
}


window.onresize = reflowLayout;

function mkbutton(text, handler, repeats){
    var button = $('<div>').text(text).addClass('control_button');
    var cell = $('<td>').addClass('control_button_cell').append(button);

    var repeater;

    var down = function(e){
        e.preventDefault();
        handler();
        button.addClass('active');
        navigator.vibrate(20);
        if(repeats){
            clearInterval(repeater);
            repeater = setInterval(handler,100);
        }
    };

    var up = function(e){
        e.preventDefault();
        button.removeClass('active');
        if(repeater){
            clearInterval(repeater);
            repeater = undefined;
        }
    };

    button.on('touchstart', down);
    button.on('mousedown', down);

    button.on('touchend', up);
    button.on('mouseup', up);
    button.on('touchcancel', up);
    button.on('mouseout', up);

    return cell;
}

function mkbuttons(controls){
    var l = mkbutton('←', controls.leftHandler, true);
    var d = mkbutton('↓', controls.spaceHandler);
    var u = mkbutton('↻', controls.upHandler);

    var r = mkbutton('→', controls.rightHandler, true);
    var row = $('<tr>');
    row.append([l,d,u,r]);
    return $('<table style="width:100%; border-collapse: collapse;"></table>').append(row);
}
function mkgrid(w,h,size,mapping){
   var table = $('<table>');
    table.css('border-collapse','collapse');
    table.css('position','fixed');
    table.css('border','1px solid black');
    table.css('table-layout','fixed');
    table.data('w',w);
    table.data('h',h);
    for (var y=0;y<h;y++){
       var row = $('<tr>');
        for (var x=0;x<w;x++){
           var cell = $('<td>');

            cell.css('width','2%');
            //cell.css('height',size+'px');
            row.append(cell);
            if(mapping){
                if(!mapping[x]){
                    mapping[x]=[];
                }
                mapping[x][y]=cell;
            }
        }
        table.append(row);
    }

    globtable=table;
    return table;
}
var mapping=[];
var cellstate=[];
var cellsolid=[];
var pieces=genpieces(4);
var lastRefreshedState=[];
function refreshD(mapping,cellstate){
    //console.log(JSON.stringify(cellstate));
    for (x in mapping){
        for (y in mapping[x]){
            var cell=mapping[x][y];
            var state=cellstate[x][y];
            var last=lastRefreshedState[x][y];
            if(state!=last){
                if(state){
                   cell.css('background-color','black');
                }else{
                    cell.css('background-color','white');
                }
                lastRefreshedState[x][y]=state;
            }
        }
    }
}
function initArr(arr,w,h){
    for (var y=0;y<h;y++){
       for (var x=0;x<w;x++){
            if(!arr[x]){
                arr[x]=[];
            }
            arr[x][y]=0;
       }
    }
   return arr;
}
function shuffle(array) {
    var counter = array.length, temp, index;

    // While there are elements in the array
    while (counter--) {
        // Pick a random index
        index = (Math.random() * (counter + 1)) | 0;

        // And swap the last element with it
        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}
function pickpiece(collection){
    shuffle(shuffle(collection)[0])[0]
}

function calcGridStats(cells){
    var totalMass=0;
    var bubbles=0;
    var stackHeight=0;
    var overhangs=0;
    var valleys=0;

    var highestPotentiallyBlockingRow=100;//off the grid below
    var finder = new PF.AStarFinder();

    for(var x=0;x<cells.length;x++){
        for(var y=0;y<cells[x].length;y++){
            var here=cells[x][y];
            if(here){
                totalMass++;
                if(stackHeight<cells[x].length-y){
                    stackHeight=cells[x].length-y;
                }

                if(y+1 <cells[x].length && cells[x][y+1]){
                    //has something below
                }else{
                    //nothing below
                    if(y+1 <cells[x].length){
                        //not the bottom row
                        if(y<highestPotentiallyBlockingRow){
                            highestPotentiallyBlockingRow=y;
                        }
                        overhangs++;
                    }
                }



            }else{
                //nothing here
                if(x-1 >=0 && cells[x-1][y]){
                    //has something to left
                    if(x+1 <cells.length && cells[x+1][y]){
                        //has something to right
                        valleys++;
                    }
                }
                if(y>highestPotentiallyBlockingRow){
                    var grid = new PF.Grid(cells.length, cells[x].length, transpose(cells));
                    var path = finder.findPath(0, 0, x, y, grid);
                    //console.log(path.length);

                    if(path.length==0){
                        bubbles++;
                    }

                }
            }


        }
    }
    return {
        bubbles:bubbles,
        totalMass:totalMass,
        stackHeight:stackHeight,
        overhangs:overhangs,
        valleys:valleys
    };
}
function healthfn(totalMass,stackHeight,bubbles,overhangs,valleys){
    return   stackHeight/totalMass  - 3*bubbles - 2*overhangs - valleys ;
}
function debug(txt){
    $('#debug').text(txt);
}
function init(){
    var w=10;var h=17;
    var nextpiece=0;
    initArr(cellstate,w,h);
    initArr(lastRefreshedState,w,h);
    initArr(cellsolid,w,h);
    var grid_table = mkgrid(w,h,10,mapping);
    $('#grid_area').append(grid_table);
    reflowLayout();
    //cellstate[1][1]=1;
    var tets = genpieces(4);
    var pieces = [1,2,3,4,4,4,4,5,6].map(function(x){return genpieces(x)});
    var currentPiece=shuffle(shuffle(pieces)[0])[0];
    var currentPos=[1,3];
    var nextEvent;
    var redraw = function(){
        refreshD(mapping,gridPaint(cellstate,currentPiece,currentPos));
    }

    var newPiece = function(){
        currentPiece=shuffle(shuffle(pieces)[0])[0];
        //currentPiece=tets[(nextpiece++)%tets.length];
        var center=getCenterOfMass(currentPiece);
        currentPos=[(w/2)-(center[0]),0];
    };

    var freezeCurrent = function(){
        cellstate=gridPaint(cellstate,currentPiece,currentPos);
        var clearResult = gravityClear(cellstate);
        cellstate=clearResult[0];
        //debug(JSON.stringify(calcGridStats(cellstate)));
    };

    var step = function(){
        clearTimeout(nextEvent);
        nextEvent='nothing';

        var nextPos=ptadd(currentPos,[0,1]);
        if(doesItCollide(cellstate,currentPiece,nextPos,w,h)){
            freezeCurrent();
            newPiece();
            if(doesItCollide(cellstate,currentPiece,currentPos,w,h)){
                initArr(cellstate,w,h);
            }
        }else{
            currentPos=nextPos;
        }
        redraw();
        nextEvent=setTimeout(step,50*(2+currentPiece.length));
    };

    var leftHandler=function(){
          if(!doesItCollide(cellstate,currentPiece,ptadd(currentPos,[-1,0]),w,h)){
              currentPos=ptadd(currentPos,[-1,0]);
              redraw();
          }
    };
    var rightHandler=function(){
      if(!doesItCollide(cellstate,currentPiece,ptadd(currentPos,[1,0]),w,h)){
          currentPos=ptadd(currentPos,[1,0]);
          redraw();
      }
    };
    var upHandler=function(){
       var center=getCenterOfMass(currentPiece);
       var rotated=currentPiece.map(function(p){return r90about(p,center)});
       var off=getDenormalAmount(rotated);
       rotated=roundPiece(normalize(rotated));

       //console.log(JSON.stringify(currentPiece));
       var newPos=ptadd(currentPos,off);
       var offPos;
       var r=0;
       var fit=0;
      while(r<w){
          offPos=ptadd(newPos,[r,0]);
          if(!doesItCollide(cellstate,rotated,offPos,w,h)){
              fit=1;
              break;
          }
          offPos=ptadd(newPos,[-r,0]);
          if(!doesItCollide(cellstate,rotated,offPos,w,h)){
              fit=1;
              break;
          }
          r++
      }
      if(fit){
           currentPiece=rotated;
           currentPos=offPos;
      }
      //console.log(JSON.stringify(currentPos));
       redraw();
    };
    var downHandler=function(){
        step();
    };
    var spaceHandler=function(){
        while(! doesItCollide(cellstate,currentPiece,ptadd(currentPos,[0,1]),w,h)){
            currentPos=ptadd(currentPos,[0,1]);
        }
        freezeCurrent();
        newPiece();
    };

    var controls = {
      leftHandler:  leftHandler,
      rightHandler: rightHandler,
      upHandler:    upHandler,
      downHandler:  downHandler,
      spaceHandler: spaceHandler
    };

    var buttons_row = $('<tr>').append($('<td>').attr('colspan', w).attr('style', ' height: 10vh; ').append( mkbuttons(controls) ));
    grid_table.append(buttons_row);

    $('body').keydown(function (e) {
      var keyCode = e.keyCode || e.which,
          arrow = {left: 37, up: 38, right: 39, down: 40 ,space: 32};

      switch (keyCode) {
        case arrow.left:
            leftHandler();
        break;
        case arrow.up:
            upHandler();
        break;
        case arrow.right:
            rightHandler();
         break;
        case arrow.down:
            downHandler();
        break;
        case arrow.space:
            spaceHandler();
        break;
      }
    });

    step();

}

init();

//$('body').text(JSON.stringify(genpieces(4)));

